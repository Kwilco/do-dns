import os
import socket
import sys

import requests


DO_API = "https://api.digitalocean.com/v2"


def get_dns_ip(domain: str) -> str:
    """ Return the IP address that DNS resolves. """
    return socket.gethostbyname(domain)


def get_public_ip() -> str:
    """ Return the actual current public IP address. """
    response = requests.get("https://api.ipify.org")
    response.raise_for_status()

    return response.text


def create_digitalocean_api_session(api_key: str) -> requests.Session:
    session = requests.Session()
    session.headers.update({
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}",
    })

    return session


def get_digitalocean_dns_records(session: requests.Session, domain: str):
    """
    Fetches DigitalOcean DNS records for the given domain.
    https://developers.digitalocean.com/documentation/v2/#list-all-domain-records
    """
    response = session.get(f"{DO_API}/domains/{domain}/records")
    response.raise_for_status()

    return response.json()["domain_records"]


def update_digitalocean_dns_a_record(session: requests.Session, domain: str, record: dict, ip: str):
    """ Updates existing DigitalOcean DNS A record with the new IP address. """

    response = session.put(
        f"{DO_API}/domains/{domain}/records/{record['id']}",
        json={
            "type": record["type"],
            "name": record["name"],
            "ttl": record["ttl"],
            "data": ip,
        },
    )
    response.raise_for_status()

    print(f"Updated {domain} A record {record['name']} with new ip {ip}")


def get_domains_from_argv() -> str:
    """ Read target domain name from argv, or politely explode if missing. """
    try:
        return sys.argv[1], sys.argv[2]
    except IndexError:
        print("Usage:  do-dns-update domain.name subdomain.domain.name", file=sys.stderr)
        sys.exit(1)


def get_api_key_from_env() -> str:
    """ Read DigitalOcean API key from environment, or politely explode if missing. """
    try:
        return os.environ["DIGITALOCEAN_API_KEY"]
    except KeyError:
        print("DIGITALOCEAN_API_KEY must be set in the environment", file=sys.stderr)
        sys.exit(1)


def main():
    domain, subdomain = get_domains_from_argv()
    api_key = get_api_key_from_env()

    dns_ip = get_dns_ip(subdomain)
    public_ip = get_public_ip()

    if dns_ip == public_ip:
        print(f"DNS record for {subdomain} matches public ip {public_ip}")
        sys.exit(0)

    print(f"DNS record for {subdomain} does not match current public ip {public_ip}, updating...")

    session = create_digitalocean_api_session(api_key)
    obsolete_a_records = [
        record
        for record in get_digitalocean_dns_records(session, domain)
        if record["type"] == "A" and record["data"] == dns_ip
    ]

    for record in obsolete_a_records:
        update_digitalocean_dns_a_record(session, domain, record, public_ip)


if __name__ == "__main__":
    main()
