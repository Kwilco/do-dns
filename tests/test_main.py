from do_dns import main as module


def test_get_dns_ip_returns_ip_looking_thing():
    ip = module.get_dns_ip("example.org")

    octets = ip.split(".")

    assert len(octets) == 4
    assert all(
        0 < int(octet) < 256
        for octet in octets
    )


def test_get_public_ip_returns_ip_looking_thing():
    ip = module.get_public_ip()

    octets = ip.split(".")

    assert len(octets) == 4
    assert all(
        0 < int(octet) < 256
        for octet in octets
    )
