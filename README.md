# do-dns

Tool to update DigitalOcean DNS A records based on current public IP.
A poorman's emulation of something like a real dynamic DNS service.


## Installation:

### Pipsi

Ideally you can install with `pipsi` to easily isolate this in a virtualenv.

```bash
pipsi install git+https://gitlab.com/Kwilco/do-dns#egg=do-dns --python=python3.6
```

Less ideally you can install directly with `pip`, but hopefully still in a
virtualenv. The only dependency is `requests`, so it shouldn't pollute your
system python too much if you must install it there.

### pip

```bash
pip3.6 install --user git+https://gitlab.com/Kwilco/do-dns#egg=do-dns
```

## How it works

This script will fetch the actual public IP address and compare it with the IP
that DNS resolves for the given subdomain. If they match, great, nothing
happens. If they don't match, this script will hit the DigitalOcean API and
update the obselete A records with your new public IP.

This command will update the A records for the given subdomain **and all other
subdomains that share the same IP address**. If you have other A records
pointing elsewhere, they should not be affected. The idea behind this is that
if you are in a situation where you have multiple A records pointing to the
same not-so-static IP, you're going to want to update all the A records that
were pointing at the now obselete IP. Rather than having to enumerate all of
those in this script's invocation (and keep it up-to-date), this script will
figure it out for you.


## Usage:

```bash
 export DIGITALOCEAN_API_KEY=keepmesecret
do-dns-update my.domain sub.my.domain
```

The first parameter is the base domain name, aka the domain that shows up in
the list of domains on DigitalOcean. This is needed so that the API knows what
domain to alter the A record(s) for.

The second parameter is used to figure out what A records you actually care
about updating. This can be the same as your base domain if you want to
target the root A record (aka `@`). If you want to target a wildcard, you can
do that easily by specifying something like `asdfbkasdfs.my.domain`, which will
resolve via the wildcard A record, thus targeting its record.

If you 100% trust your host and aren't worried about anyone looking at your API
key in the process list in `top`, or reading it out of a crontab, etc, you can
do this in one line like:

```bash
env DIGITAL_OCEAN_API_KEY=secretsecret do-dns-update my.domain sub.my.domain
```

This also has a Docker container and a script to run the `do-dns-update`
command in an infinite loop on an interval. If you are using Docker Compose,
you can do something like this:

```yaml
do_dns:
  image: "registry.gitlab.com/kwilco/do-dns:latest"
  command: "my.domain my.domain 300"
  environment:
    DIGITALOCEAN_API_KEY: "KEEP_ME_SECRET"
```

The first two params in the command are the args to `do-dns-update`, while the
third is the number of seconds to wait between update checks. This should
probably match the TTL on the A record, since it won't update any faster than
that. It is safe to run faster though, since this script only hits the
DigitalOcean API at all when it needs to update A records (and doesn't need to
do hit it to see if they need updating).
