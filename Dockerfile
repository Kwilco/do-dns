FROM python:alpine

WORKDIR /app
ADD . /app
RUN pip install .

ENTRYPOINT ["/app/run.sh"]
