import setuptools


setuptools.setup(
    name='do-dns',
    version='0.1.0',
    description='Script to update Digital Ocean DNS records based on current public IP.',
    author='Kyle Wilcox',
    author_email='k.j.wilcox@gmail.com',
    packages=setuptools.find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'do-dns-update=do_dns.main:main',
        ],
    },
    install_requires=(
        'requests',
    ),
)
